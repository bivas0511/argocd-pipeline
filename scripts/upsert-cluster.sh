#!/bin/bash

set -ea

kubectl config set-credentials "$CI_PROJECT_ID" --token="$KUBE_TOKEN"
kubectl config set-context "$CI_PROJECT_ID" --cluster="$CI_PROJECT_ID" --user="$CI_PROJECT_ID"
kubectl config use-context "$CI_PROJECT_ID"
# show versions for debug

kubectl version
helm version

# Install CRDs, ArgoCD

echo "Add repositories"
helm repo --username=gitlab-ci-token --password=$CI_JOB_TOKEN add managed-crds https://gitlab.com/api/v4/projects/47132210/packages/helm/stable
helm repo update

echo "Install/update CRDs"
helm fetch --version $MANAGED_CRDS_VERSION managed-crds/managed-crds --untar

# install all the CRDS in the template folder
for folder in managed-crds/templates/*/ ; do
	kubectl apply -f $folder
done

echo "Apply namespaces"
helm repo --username=gitlab-ci-token --password=$CI_JOB_TOKEN add managed-namespaces https://gitlab.com/api/v4/projects/47259592/packages/helm/stable
helm template --version $MANAGED_NAMESPACES managed-namespaces/managed-namespaces -f $TARGET | kubectl apply -f -

echo "Install/Update Sealed-secrets with CRDs"
helm repo --username=gitlab-ci-token --password=$CI_JOB_TOKEN add bitnami-labs https://gitlab.com/api/v4/projects/46954432/packages/helm/stable
helm upgrade --version $MANAGED_SEALED_SECRETS --install sealed-secrets bitnami-labs/sealed-secrets -n sealed-secrets

echo "Install/update ArgoCD"
helm repo --username=gitlab-ci-token --password=$CI_JOB_TOKEN add managed-argocd https://gitlab.com/api/v4/projects/47132311/packages/helm/stable
helm upgrade --version $MANAGED_ARGOCD_VERSION --install argocd managed-argocd/managed-argocd -n argocd --skip-crds

echo "Install/update cert-manager"
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.11.0/cert-manager.yaml
sleep 120
